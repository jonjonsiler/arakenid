@extends('layouts.default')
@section('content')
        <h1>Edit Site</h1>
        <hr/>
	@include('errors._list')
        {!! Form::open(['url'=>'admin/sites']) !!}
	    @include('admin.sites._form', ["submitLabel"=>"Add Site"])
        {!! Form::close() !!}
@stop