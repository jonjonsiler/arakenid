           <div class="form-group">
                {!! Form::label('title', "Title:") !!}
                {!! Form::text('title',null, ['class'=>'form-control']) !!}
	    </div>
	    <div class="form-group">
                {!! Form::label('primary_domain', "Primary domain URL:") !!}
                {!! Form::text('primary_domain', null, ['class'=>'form-control']) !!}
	    </div>
	
	    <div class="form-group">
                {!! Form::label('description', "Short Description") !!}
                {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
	    </div>
	
	    <div class="form-group">
                {!! Form::label('depth', "Depth of Index") !!}
                {!! Form::text('depth', null, ['class'=>'form-control']) !!}
	    </div>
	
	    <div class="form-group">
                {!! Form::label('required', "Always follow") !!}
                {!! Form::textarea('required', null, ['class'=>'form-control']) !!}
	    </div>
	
	    <div class="form-group">
                {!! Form::label('disallowed', "Always ignore") !!}
                {!! Form::textarea('disallowed', null, ['class'=>'form-control']) !!}
            </div>

	    <div class="form-group">
                {!! Form::label('can_leave_domain', "Can robot leave domain?") !!}
                <div class="radio">
		    {!! Form::radio('can_leave_domain', 1, ['id'=>'can_leave_domain_yes', 'class'=>'form-control']) !!}
		    {!! Form::label('can_leave_domain_yes', "Yes") !!}
		</div>
		<div class="radio">
		    {!! Form::radio('can_leave_domain', 0, ['id'=>'can_leave_domain_no','class'=>'form-control']) !!}
		    {!! Form::label('can_leave_domain_no', "No") !!}
		</div>
            </div>
	<div class="form-group">
	    {!! Form::submit($submitLabel, ['class'=>'btn btn-primary form-control']) !!}
	</div>