@extends('layouts.default')
@section('content')
@if (count($sites))
	<h1> Administrative view</h1>
	<table class="table table-striped">
		<caption>All Sites</caption>
		<thead>
			<tr>
				<th>Title</th>
				<th>URL</th>
				<th>Last Indexed</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($sites as $site)
			<tr>
				<td><a href="{{ url('admin/sites', $site->id) }}"> {{ $site->title }}</a></td>
				<td>{{ $site->primary_domain }}</td>
				<td>{{ $site->index_date }}</td>
				<td>{!! link_to_action('Admin\SitesController@edit', "Edit", [$site->id] , ["class"=>"btn btn-default"] ) !!}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@endif
@stop
	