@extends('layouts/default')
@section('content')
<h1>Links</h1>
@if (count($links))
	<table class="table table-striped">
		<caption>All Links</caption>
		<thead>
			<tr>
				<th>Title</th>
				<th>URL</th>
				<th>Last Indexed</th>
				<th>Size</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
            @foreach($links as $link)
			<tr>
				<td><a href="{{ url('admin/links/', $link->site->id) }}"> {{$link->site->title}}</a></td>
				<td>{{ $link->site->primary_domain . "/" . $link->url }}</td>
				<td>{{ $link->index_date }}</td>
				<td>{{ $link->size }}</td>
				<td>{!! link_to_action('Admin\LinksController@edit', "Edit", [$link->id] , ["class"=>"btn btn-default"] ) !!}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
{!! $links->render() !!}
@else
<div class="alert alert-info alert-dismissable">
    <p>There were no links loaded</p>
</div>
@endif
@stop