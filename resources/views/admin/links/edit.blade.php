@extends('layouts.default')
@section('content')
@if ($site)
	<h1>Edit Site</h1>
        <hr/>
	@include('errors._list')
        {!! Form::model($site, ['method'=>'PATCH', 'url'=>'admin/sites', $site->id, 'action'=>['Admin\SitesController@update']]) !!}
	    @include('admin.sites._form', ["submitLabel"=>"Update Site"])
        {!! Form::close() !!}
@endif
@stop
	