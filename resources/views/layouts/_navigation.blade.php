		 <!-- Static navbar -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
				  <div class="navbar-header">
				    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				      <span class="sr-only">Toggle navigation</span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				    </button>
				    <a class="navbar-brand" href="#">Arakenid</a>
				  </div>
				  <div id="navbar" class="navbar-collapse collapse">
				    <ul class="nav navbar-nav">
				      <li class="active"><a href="/admin">Home</a></li>
				      <li><a href="/admin/sites">Sites</a></li>
				      <li><a href="/admin/keywords">Keywords</a></li>
				      <li class="dropdown">
				        <a href="/admin/links" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Links <span class="caret"></span></a>
				        <ul class="dropdown-menu" role="menu">
				          <li><a href="#">Action</a></li>
				          <li><a href="#">Another action</a></li>
				          <li><a href="#">Something else here</a></li>
				          <li class="divider"></li>
				          <li class="dropdown-header">Nav header</li>
				          <li><a href="#">Separated link</a></li>
				          <li><a href="#">One more separated link</a></li>
				        </ul>
				      </li>
				    </ul>
				    <ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Register</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				    </ul>
				  </div><!--/.nav-collapse -->
				</div><!--/.container-fluid -->
			</nav>
	