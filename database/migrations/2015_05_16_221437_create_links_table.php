<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('links', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('site_id');
			$table->longText('url');
			$table->string('title');
			$table->longText('description');
			$table->text('full_text');
			$table->dateTime('index_date');
			$table->decimal('size')->places("2");
			$table->string('hash');
			$table->tinyInteger('visible');
			$table->integer('level');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('links');
	}

}
