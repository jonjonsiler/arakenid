<?php
/**
 * Sites factory
 * Create a site with some basic information
 */
$factory->define(App\Site::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->company,
        'primary_domain' => $faker->domainName,
        'description' => $faker->catchPhrase,
    ];
});

/**
 * Links factory
 * Creates and binds links to sites
 */
$factory->define(App\Links::class, function ($faker) {
    // Retreive all site_id's
    $sites = App\Site::all()->lists('id')->toArray();
    return [
		'url' => $faker->slug,
        'site_id' => $faker->randomElement($sites),
		"title" => $faker->words(7, true),
        "description" => $faker->realText(500),
        "full_text" => $faker->text(500),
        "index_date" => $faker->dateTime,
        "size"  => $faker->randomFloat(2, 1, 128), //decimal
        "hash" => $faker->md5,
        "visible" => 1, //boolean
        "level" => $faker->biasedNumberBetween(0, 2, 'sqrt'),
    ];
});

/**
 * Keywords factory
 * Creates a database of found kewords and stores them by hash
 */
$factory->define(App\Keywords::class, function ($faker) {
    return [

    ];
});