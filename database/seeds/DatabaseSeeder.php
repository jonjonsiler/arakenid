<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

	    //$this->call(UsersTableSeeder::class);
    	
    	// Truncate the table
    	// App\Site::truncate();
    	$this->call(SitesTableSeeder::class);
    	
    	// Truncate the table
    	// App\Links::truncate();
    	$this->call(LinksTableSeeder::class);

    	//$this->call(KeywordsTableSeeder::class);

    	Model::reguard();	
	}

}
