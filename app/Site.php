<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sites';
	
	protected $fillable = [
		'title',
		'primary_domain',
		'description',
		'required',
		'disallow',
		'depth',
	];

	public function links()
	{
	    return $this->hasMany("App\Links");
	}
}
