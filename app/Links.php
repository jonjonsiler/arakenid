<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Links extends Model {

	protected $table = "links";

	// Either guarded or fillable for mass assignment
	protected $guarded = ['id'];
	
	public function __construct($attributes = array())
	{
    	parent::__construct($attributes);
	}

	
	/**
	 * Go through the list of links and remove the entries 
	 * that are blocked by robots.txt
	 */
	protected function verifyLinks()
	{
	    
	}
	
	public function site()
	{
	    return $this->belongsTo('App\Site');
	}
	
	// public function keywords()
	// {
	//   // return $this->hasMany();
	// }
}
