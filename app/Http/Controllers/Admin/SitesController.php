<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Site;
use App\Links;

//use Request;
use App\Http\Requests\SiteRequest;

class SitesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// Could be Site::older
		$sites = Site::latest("index_date")->get();
		return view('admin.sites.index', compact( 'sites' ));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
//          $site = Site::create();
            return view('admin.sites.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param SiteRequest  $request
	 * @return Response
	 */
	public function store(SiteRequest $request)
	{
		Site::create($request->all());
		return redirect('admin/sites');
	}

	/**
	 * Crawl a site
	 *
	 * Iterate through all the links on a page, adding all links as we go
	 * Checking each link recursively and adding to the links table.
	 * 
	 * @param Site  $site
	 * @return Response
	 */
	public function crawl(Site $site){
	    //Get base URL
	    $url = "http://www.back40design.com/";
	    
	    $page = new PageController($url);
	    //get first level of links
	    $page->getPage();

	    // determine if the links need to be added to the database
	    // robots and nofollow verfication
	    
	    //get first level links and parse them for links and text
	    
	    //add them to the database
	    
	    //$response = $request->send();
	    //dd($site);
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  Site	 $site
	 * @return Response
	 */
	public function show(Site $site)
	{
	    return view('admin.sites.show', compact( 'site' ));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  Site  $site
	 * @return Response
	 */
	public function edit(Site $site)
	{
	    return view('admin.sites.edit', compact( 'site' ));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Site  $site
	 * @return Response
	 */
	public function update(Site $site, SiteRequest $request)
	{
	    $site->update($request->all());
	    return view('admin.sites.edit', compact( 'site' ));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  Site  $site
	 * @return Response
	 */
	public function destroy(Site $site)
	{
		//
	}

}
