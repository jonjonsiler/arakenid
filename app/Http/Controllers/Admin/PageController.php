<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

// use Illuminate\Http\Request;
use Httpful\Request;

class PageController extends Controller {

	public function __construct($url){
	    $this->domain =  parse_url($url, PHP_URL_HOST);
	    // configure the request for this controller
	    $this->template = Request::init()
		    ->addHeader("User-Agent", "Back40Bot") // This will be changed later to a setting of the user's choice
		    ->uri($url)
		    ->expectsHtml(); 
	    //Look up site?
	    Request::ini($this->template);
	    //dd($this->template);
	}
	
	/**
	 * Get page contents and parse the data
	 */
	public function getPage()
	{	    
	    $this->response = $this->template->sendIt();
	    $this->hash = md5($this->response->body);

	    //Prioritize structural elements
	    // $this->prioritize();
	    
	    //Get metadata and assign a weight
	    
	    //get a text only profile of the page for keyword mapping
	    $this->reduce();
	    
	    //get all links from the page
	    $this->getLinks();
	    
	    dd($this);
	}
	/**
	 * Grab all internal links from the site and create a list to work from
	 * 
	 * @return boolean
	 */
	protected function getLinks()
	{
	    $body = $this->response->body;
	    preg_match_all("/<a[^>]*href\s*=\s*[\'\"]?([+:%\/\?~=&;\\\(\),._a-zA-Z0-9-]*)(#[.a-zA-Z0-9-]*)?[\'\" ]?(\s*rel\s*=\s*[\'\"]?(nofollow)[\'\"]?)?/i", $body, $regs, PREG_SET_ORDER);
	    $links = [];
	    foreach ($regs as $val) {
		$linkURL = $val[1];
		$linkDomain = parse_url($linkURL, PHP_URL_HOST);
		$linkPath = parse_url($linkURL, PHP_URL_PATH);
		if($linkURL == "" || $linkURL == "/")
		{
		    continue;
		}
		//is URL in the same domain or a relative URL
		if($linkDomain !== $this->domain){
		    continue;
		}
		// is URL path already in the link list
		if(array_search($linkPath, $links) == 0)
		{
		    $links[] = $linkPath;
		}
	    }
	    $this->links = $links;
	    return true;
	}

	/**
	 * Get structurals (h1-h6, em, strong) and give them weight
	 */
	protected function prioritize()
	{
	    
	}
	
	/**
	 * Get the raw text of the page from the body element 
	 * removing all HTML markup and reducing it to just 
	 * the raw keyword profile.
	 */
	protected function reduce()
	{
	    //remove script tags and javascript
	    
	    // remove HTML tags
	    $fulltext = strip_tags($this->response->body);
	    
	    //reduce text content
	    $fulltext = preg_replace("/[\s]+/m", " ", $fulltext);
	    $this->fulltext = $fulltext;
	}
}
