<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');
Route::get('admin', 'AdminController@index');

// Site route handlers for search
//Route::get('admin/sites', 'Admin\SitesController@index');
//Route::get('admin/sites/create', 'Admin\SitesController@create');
//Route::get('admin/sites/{id}', 'Admin\SitesController@show');
//Route::get('admin/sites/edit/{id}', 'Admin\SitesController@edit');
//Route::post('admin/sites', 'Admin\SitesController@store');
Route::resource("admin/sites", "Admin\SitesController");
Route::get('admin/sites/crawl/{sites}', 'Admin\SitesController@crawl');

// Links route handlers for search
Route::get('admin/links', 'Admin\LinksController@index');
Route::get('admin/links/edit', 'Admin\LinksController@edit');
Route::get('admin/links/create', 'Admin\LinksController@create');

// A collection of links by site
Route::get('admin/links/{site_id}', 'Admin\LinksController@index');
Route::get('admin/links/show/{link_id}', 'Admin\LinksController@show');


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
// 	'admin' => 'AdminController'
]);
